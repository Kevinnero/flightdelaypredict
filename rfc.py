#從full_data_flightdelay選取特徵，跑演算法
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.decomposition import PCA


stdsc = StandardScaler()

data = pd.read_csv( 'initdata/full_data_flightdelay.csv' )

np.random.seed(900510)
rn=np.random.randint(len(data), size=500000, dtype=int)

data = data.loc[ rn , : ] 

TIME_BLK_mapping = { '0000-0059':0 , '0100-0159':1 , '0200-0259':2 , '0300-0359':3 , '0400-0459':4 , '0500-0559':5 
                   , '0600-0659':6 , '0700-0759':7 , '0800-0859':8 , '0900-0959':9 , '1000-1059':10 , '1100-1159':11 
                   , '1200-1259':12 , '1300-1359':13 , '1400-1459':14 , '1500-1559':15 , '1600-1659':16 , '1700-1759':17 
                   , '1800-1859':18 , '1900-1959':19 , '2000-2059':20 , '2100-2159':21 , '2200-2259':22 , '2300-2359':23 
                    }

data["DEP_TIME_BLK"] = data["DEP_TIME_BLK"].map(TIME_BLK_mapping)

data = data.dropna()


data_1 = data.iloc[ : , [ 0,1,3,4,5,6,7,9,10,11,12,13,14,15,16,18,19,21,22,23,24,25 ] ]
data_1 = data.loc[:, ['DISTANCE_GROUP','CONCURRENT_FLIGHTS','SNOW','DAY_OF_WEEK','AIRLINE_AIRPORT_FLIGHTS_MONTH']]
data_2 = data.iloc[ : , [2]]


X , y = data_1 , data_2

X_train , X_test , y_train , y_test = train_test_split( X , y , test_size=0.3 , random_state=0 , stratify=y )

X_train_std = stdsc.fit_transform(X_train)
X_test_std = stdsc.transform(X_test)

feat_labels = data_1.columns[1:]
# pipe = make_pipeline(PCA(n_components=5), RandomForestClassifier( n_estimators=100, max_depth=6 , criterion = 'gini', class_weight='balanced'))
pipe = make_pipeline( PCA(n_components=2), LogisticRegression(penalty='l2',C=100,solver='liblinear',multi_class='ovr',random_state=666))

# randomForestModel = RandomForestClassifier( n_estimators=100, max_depth=6 , criterion = 'gini', class_weight='balanced')
# randomForestModel.fit(X_train_std, y_train)
pipe.fit(X_train_std, y_train)


y_pred = pipe.predict(X_test)
confmat = confusion_matrix(y_true=y_test, y_pred=y_pred)

# Setting the attributes
fig, ax = plt.subplots(figsize=(2.5, 2.5))
ax.matshow( confmat , cmap=plt.cm.Blues , alpha=0.3 )
for i in range( confmat.shape[0] ) :
    for j in range( confmat.shape[1] ) :
        ax.text( x=j , y=i , s=confmat[ i , j ] , va='center' , ha='center' )

# Sets the labels
plt.xlabel( 'Predicted Label' ) ##, fontsize=16
plt.ylabel( 'True Label' ) ##, fontsize=16
plt.show()


############ find importance feature
# importances = randomForestModel.feature_importances_
# indices = np.argsort(importances)[::-1]

# for f in range(data_1.shape[1]):
#     try:
#         print("%2d) %-*s %f" % (f+1, 30, feat_labels[indices[f]], importances[indices[f]]))
#     except:
#         continue

# plt.title('Feature Importance')
# plt.bar(range(data_1.shape[1]), importances[indices],align='center')
# plt.xticks(range(data_1.shape[1]), feat_labels, rotation=90)
# plt.xlim([-1,10])
# plt.tight_layout()
# plt.show()

############ loop for find parameter
# weights, params ,scorex,scorey= [], [], [], []

# for c in np.arange(5, 10):
#     randomForestModel = RandomForestClassifier( n_estimators=500, max_depth=c , criterion = 'gini' )

#     randomForestModel.fit(X_train_std, y_train)
#     scorex.append(randomForestModel.score(X_test_std,y_test))
#     scorey.append(randomForestModel.score(X_train_std,y_train))
#     params.append(c)
#     print(c)

# plt.plot(params,scorex, color='red')
# plt.plot(params, scorey)
# # plt.ylim([0.8, 1.0])
# # plt.xscale('log')
# plt.tight_layout()
# plt.show()

# predicted = randomForestModel.predict(X_train_std)

# print('訓練集: ',randomForestModel.score(X_train_std,y_train))
# print('測試集: ',randomForestModel.score(X_test_std,y_test))

predicted = pipe.predict(X_train_std)

print('訓練集: ',pipe.score(X_train_std,y_train))
print('測試集: ',pipe.score(X_test_std,y_test))





# print('\033[1;37m我是大帥哥\n\
#     ░░░░░░░░▄░░░░░░░░░░░░░░▄\n\
#     ░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌\n\
#     ░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐\n\
#     ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐\n\
#     ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐\n\
#     ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌\n\
#     ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌\n\
#     ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐\n\
#     ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌\n\
#     ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌\n\
#     ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒')







