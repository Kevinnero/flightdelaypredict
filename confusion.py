from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.svm import SVC
import matplotlib.pylab as plt
from sklearn.preprocessing import StandardScaler

stdsc = StandardScaler()
pipe_svc = make_pipeline( StandardScaler() , SVC(random_state=1) )

pipe_svc.fit(X_train, y_train)
y_pred = pipe_svc.predict(X_test)
confmat = confusion_matrix(y_true=y_test, y_pred=y_pred)


# Setting the attributes
fig, ax = plt.subplots(figsize=(2.5, 2.5))
ax.matshow( confmat , cmap=plt.cm.Blues , alpha=0.3 )
for i in range( confmat.shape[0] ) :
    for j in range( confmat.shape[1] ) :
        ax.text( x=j , y=i , s=confmat[ i , j ] , va='center' , ha='center' )

# Sets the labels
plt.xlabel( 'Predicted Label' ) ##, fontsize=16
plt.ylabel( 'True Label' ) ##, fontsize=16
plt.show()