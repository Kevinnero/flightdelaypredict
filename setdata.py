import pandas as pd
import numpy as np

monthlist = ['01','02','03','04','05','06','07','08','09','10','11','12']
full_data = pd.DataFrame()
airportslist = pd.read_csv('initdata/raw_data/airports_list.csv')
weatherdata = pd.read_csv('initdata/raw_data/airport_weather_2019.csv')
np.random.seed(0)
datanum = 500

for i in range(12):
    df = pd.read_csv(f"initdata/raw_data/ONTIME_REPORTING_{monthlist[i]}.csv")
    rn=np.random.randint(len(df), size=datanum, dtype=int)

    newdf = df.loc[rn, ['MONTH', 'DAY_OF_MONTH', 'ORIGIN_AIRPORT_ID', 'DEST_AIRPORT_ID','DEP_TIME_BLK','ARR_TIME_BLK','DISTANCE','DEP_DELAY_NEW', 'ARR_DELAY_NEW']]
    newdf = newdf.reset_index().drop(['index'],axis=1)

    airportname = []
    airportname2 = []
    dayname = []
    dayname2 = []
    tmax = []
    snow = []
    awnd = []
    snwd = []
    prcp = []

    for j in range(datanum):
        found = False
        for k in range(97):
            if (newdf.at[j,'ORIGIN_AIRPORT_ID']==airportslist.at[k,'ORIGIN_AIRPORT_ID'] and (not found) and airportslist.at[k,'NAME']!=''):
                airportname.append(airportslist.at[k,'NAME'])
                found = True
        if (found==False):
            airportname.append(np.nan)

        dayname.append(f"{newdf.at[j,'MONTH']}/{newdf.at[j,'DAY_OF_MONTH']}/2019")
        dayname2.append(f"2019-{newdf.at[j,'MONTH']}-{newdf.at[j,'DAY_OF_MONTH']}")
    newdf.insert(int(newdf.count(axis='columns')[0]),column='NAME',value=airportname)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='daytype1',value=dayname)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='daytype2',value=dayname2)
    # newdf = newdf.dropna()

    for m in range(datanum):
        found = False
        n = 0
        while (n<38675):
            if (newdf.at[m,'NAME']!=weatherdata.at[n,'NAME']):
                n+=364
                # print("1")
            elif (newdf.at[m,'NAME']==weatherdata.at[n,'NAME'] and (newdf.at[m,'daytype1']==weatherdata.at[n,'DATE'] or newdf.at[m,'daytype2']==weatherdata.at[n,'DATE'])):
                if (weatherdata.at[n,'TMAX']!='' or weatherdata.at[n,'TMAX']!='nan'):
                    tmax.append(weatherdata.at[n,'TMAX'])
                else:
                    tmax.append(np.nan)
                if (weatherdata.at[n,'SNOW']!='' or weatherdata.at[n,'SNOW']!='nan'):
                    snow.append(weatherdata.at[n,'SNOW'])
                else:
                    snow.append(np.nan)
                found = True
                break
            n+=1
        if (found==False):
            tmax.append(np.nan)
            snow.append(np.nan)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='TMAX',value=tmax)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='SNOW',value=snow)
    # newdf = newdf.dropna()

    for j in range(datanum):
        found = False
        for k in range(97):
            if (newdf.at[j,'DEST_AIRPORT_ID']==airportslist.at[k,'ORIGIN_AIRPORT_ID'] and (not found) and airportslist.at[k,'NAME']!=''):
                airportname2.append(airportslist.at[k,'NAME'])
                found = True
        if (found==False):
            airportname2.append(np.nan)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='NAME2',value=airportname2)
    # newdf = newdf.dropna()

    for m in range(datanum):
        found = False
        n = 0
        while (n<38675):
            if (newdf.at[m,'NAME2']!=weatherdata.at[n,'NAME']):
                n+=364
            elif (newdf.at[m,'NAME2']==weatherdata.at[n,'NAME'] and (newdf.at[m,'daytype1']==weatherdata.at[n,'DATE'] or newdf.at[m,'daytype2']==weatherdata.at[n,'DATE'])):
                if (weatherdata.at[n,'AWND']!='' or weatherdata.at[n,'AWND']!='nan'):
                    awnd.append(weatherdata.at[n,'AWND'])
                else:
                    awnd.append(np.nan)
                if (weatherdata.at[n,'SNWD']!='' or weatherdata.at[n,'SNWD']!='nan'):
                    snwd.append(weatherdata.at[n,'SNWD'])
                else:
                    snwd.append(np.nan)
                found = True
                if (weatherdata.at[n,'PRCP']!='' or weatherdata.at[n,'PRCP']!='nan'):
                    prcp.append(weatherdata.at[n,'PRCP'])
                else:
                    prcp.append(np.nan)
                found = True
                break
            n+=1
        if (found==False):
            awnd.append(np.nan)
            snwd.append(np.nan)
            prcp.append(np.nan)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='AWND',value=awnd)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='SNWD',value=snwd)
    newdf.insert(int(newdf.count(axis='columns')[0]),column='PRCP',value=prcp)

    full_data = pd.concat([full_data,newdf])
full_data = full_data.dropna().drop(['DAY_OF_MONTH','daytype1','daytype2'],axis=1)

full_data.to_csv('test.csv')


# print (full_data.at[7,'SNOW'])


