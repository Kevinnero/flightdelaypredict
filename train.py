import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from models.raschka import plot_decision_regions
from sklearn.feature_selection import SelectFromModel
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier

np.random.seed(10)

df = pd.read_csv('initdata/full_data_flightdelay.csv')
df = df.drop(['CARRIER_NAME','DEP_TIME_BLK','DEPARTING_AIRPORT','PREVIOUS_AIRPORT'],axis=1)
x ,y = df.drop(['DEP_DEL15'],axis=1),df.iloc[:,2].values
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=666, stratify=y)
stdsc = StandardScaler()

x_train_std = stdsc.fit_transform(x_train)
x_test_std = stdsc.transform(x_test)
lr_pipe = make_pipeline(StandardScaler(),LogisticRegression(penalty='l1',solver='liblinear',multi_class='ovr',random_state=666))
lr = LogisticRegression(penalty='l1',C=100,solver='liblinear',multi_class='ovr',random_state=666)
# lr.fit(x_train_std,y_train)
# sfs = SelectFromModel(lr, threshold=0.1)
# xse = sfs.transform(x_train_std) 


# for  f in range(xse.shape[1]):
#     print("%2d) %-*s %f" % (f+1, 30, feat_labels[indices[f]]))
weights, params ,scorex,scorey= [], [], [], []

# sfs.fit(x_train_std, y_train)
for c in np.arange(0, 5.):
    lr = LogisticRegression(C=10.**c,penalty='l1',solver='liblinear',multi_class='ovr',random_state=666)
    lr.fit(x_train_std,y_train)
    # y_pred = lr.predict(x_test)
    scorex.append(lr.score(x_test_std,y_test))
    scorey.append(lr.score(x_train_std,y_train))
    # weights.append(lr.coef_[1])
    params.append(10.**c)

# plt.plot(params,scorex, color='red')
# plt.plot(params, scorey)
# # plt.ylim([0.8, 1.0])
# plt.xscale('log')
# plt.tight_layout()
# plt.show()


# weights = np.array(weights)
