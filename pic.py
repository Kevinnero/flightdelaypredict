import pandas as pd
import matplotlib.pylab as plt
import seaborn

df = pd.read_csv('Dataenc.csv')

# TIME_BLK_mapping = { '0000-0059':0 , '0100-0159':1 , '0200-0259':2 , '0300-0359':3 , '0400-0459':4 , '0500-0559':5 
#                    , '0600-0659':6 , '0700-0759':7 , '0800-0859':8 , '0900-0959':9 , '1000-1059':10 , '1100-1159':11 
#                    , '1200-1259':12 , '1300-1359':13 , '1400-1459':14 , '1500-1559':15 , '1600-1659':16 , '1700-1759':17 
#                    , '1800-1859':18 , '1900-1959':19 , '2000-2059':20 , '2100-2159':21 , '2200-2259':22 , '2300-2359':23 
#                     }


# df["DEP_TIME_BLK"] = df["DEP_TIME_BLK"].map(TIME_BLK_mapping)
# df["ARR_TIME_BLK"] = df["ARR_TIME_BLK"].map(TIME_BLK_mapping)
x = df['DEP_TIME_BLK']
y = df['ARR_DELAY_NEW']
seaborn.jointplot(data=df,y=df['ORIGIN'],x=df['ARR_DELAY_NEW'])
# plt.plot(x,y,'.')
plt.tight_layout()
plt.show()

